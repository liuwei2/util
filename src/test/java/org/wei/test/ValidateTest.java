package org.wei.test;

import org.junit.Assert;
import org.junit.Test;
import org.wei.utils.validate.BatchRule;
import org.wei.utils.validate.BatchValidator;
import org.wei.utils.validate.CountRule;
import org.wei.utils.validate.EqualsRule;
import org.wei.utils.validate.IsNumberRule;
import org.wei.utils.validate.NotEmptyRule;
import org.wei.utils.validate.ValiResult;
import org.wei.utils.validate.Validator;

public class ValidateTest {
	@Test
	public void test() {
		Validator validator = Validator.create(
				"12a",
				new BatchRule(new NotEmptyRule("不能为空")).and(
						new IsNumberRule("只能是数字"))
						.and(new CountRule(3, "只能3位")));
		ValiResult result = validator.validate();
		Assert.assertFalse(result.isPass());
		Assert.assertEquals("只能是数字", result.getInfo());
	}

	@Test
	public void test2() {
		Validator validator = Validator.create(
				"123",
				new BatchRule(new NotEmptyRule("不能为空")).and(
						new IsNumberRule("只能是数字"))
						.and(new CountRule(3, "只能3位")));
		ValiResult result = validator.validate();
		Assert.assertTrue(result.isPass());
		Assert.assertEquals(null, result.getInfo());
	}

	@Test
	public void test3() {
		Validator validator = Validator.create(
				"1234",
				new BatchRule(new NotEmptyRule("不能为空")).and(
						new IsNumberRule("只能是数字"))
						.and(new CountRule(3, "只能3位")));
		ValiResult result = validator.validate();
		Assert.assertFalse(result.isPass());
		Assert.assertEquals("只能3位", result.getInfo());
	}

	@Test
	public void test4() {
		Validator validator = Validator.create(
				null,
				new BatchRule(new NotEmptyRule("不能为空")).and(
						new IsNumberRule("只能是数字"))
						.and(new CountRule(3, "只能3位")));
		ValiResult result = validator.validate();
		Assert.assertFalse(result.isPass());
		Assert.assertEquals("不能为空", result.getInfo());
	}

	@Test
	public void test5() {
		Validator validator = Validator.create(null, new BatchRule(
				new IsNumberRule("只能是数字")).and(new CountRule(3, "只能3位")));
		ValiResult result = validator.validate();
		Assert.assertFalse(result.isPass());
		Assert.assertEquals("只能是数字", result.getInfo());
	}

	@Test
	public void test6() {
		Validator validator = Validator.create("1234", new BatchRule(
				new IsNumberRule("只能是数字")).and(new CountRule(3, "只能3位")));
		ValiResult result = validator.validate();
		Assert.assertFalse(result.isPass());
		Assert.assertEquals("只能3位", result.getInfo());
	}

	@Test
	public void test7() {
		Validator validator = Validator.create("123", new BatchRule(
				new IsNumberRule("只能是数字")).and(new CountRule(3, "只能3位")));
		Validator validator2 = Validator.create("1234", new BatchRule(
				new IsNumberRule("只能是数字")).and(new CountRule(3, "只能3位")));
		ValiResult result = BatchValidator.create(validator)
				.addValidator(validator2).validate();
		Assert.assertFalse(result.isPass());
	}

	@Test
	public void test8() {
		Validator validator = Validator
				.create("123", new IsNumberRule("只能是数字"));
		ValiResult result = validator.validate();
		Assert.assertTrue(result.isPass());
	}

	@Test
	public void test9() {
		Validator validator = Validator.create("123a",
				new IsNumberRule("只能是数字"));
		ValiResult result = validator.validate();
		Assert.assertFalse(result.isPass());
	}

	@Test
	public void test10() {
		Validator validator = Validator.create("123a", new EqualsRule(null,
				"要相等"));
		ValiResult result = validator.validate();
		Assert.assertFalse(result.isPass());
	}

	@Test
	public void test11() {
		Validator validator = Validator.create(null, new EqualsRule(null,
				"要相等"));
		ValiResult result = validator.validate();
		Assert.assertTrue(result.isPass());
	}

	@Test
	public void test12() {
		Validator validator = Validator.create("12", new EqualsRule("12",
				"要相等"));
		ValiResult result = validator.validate();
		Assert.assertTrue(result.isPass());
	}
}
