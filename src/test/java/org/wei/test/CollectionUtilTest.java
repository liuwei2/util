package org.wei.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.wei.utils.CollectionUtil;

public class CollectionUtilTest {
	@Test
	public void test() {
		CollectionUtil<Integer> collectionUtil = new CollectionUtil<Integer>();
		List<Integer> list = new ArrayList<Integer>();
		list.add(7);
		list.add(3);
		list.add(1);
		list.add(4);
		list.add(8);
		List<Integer> list2 = new ArrayList<Integer>();
		list2.add(5);
		list2.add(6);
		list2.add(10);
		list2.add(2);
		list2.add(9);
		Collection<Integer> r = collectionUtil.addToFixSize(list, list2, 4,
				new Comparator<Integer>() {
					@Override
					public int compare(Integer i, Integer i2) {
						return i - i2;
					}
				});
		Integer[] arr = new Integer[4];
		r.toArray(arr);
		Assert.assertArrayEquals(new Integer[] { 1, 2, 3, 4 }, arr);
	}
}
