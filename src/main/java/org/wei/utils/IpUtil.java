package org.wei.utils;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class IpUtil {

	public static void main(String[] args) {
		System.out.println(getPublicIpAddress());
	}

	public static String getPublicIpAddress() {
		String res = null;
		try {
			String localhost = InetAddress.getLocalHost().getHostAddress();
			Enumeration<NetworkInterface> e = NetworkInterface
					.getNetworkInterfaces();
			while (e.hasMoreElements()) {
				NetworkInterface ni = (NetworkInterface) e.nextElement();
				if (ni.isLoopback())
					continue;
				if (ni.isPointToPoint())
					continue;
				Enumeration<InetAddress> addresses = ni.getInetAddresses();
				while (addresses.hasMoreElements()) {
					InetAddress address = (InetAddress) addresses.nextElement();
					if (address instanceof Inet4Address) {
						String ip = address.getHostAddress();
						if (!ip.equals(localhost))
							res = ip;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "http://" + res;
	}
}
