package org.wei.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class IOUtil {
	public static void copy(File file, String saveTo) throws Exception {
		FileOutputStream fos = null;
		FileInputStream fis = null;
		try {
			fos = new FileOutputStream(saveTo);
			fis = new FileInputStream(file);
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = fis.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			close(fos, fis);
		}
	}

	public static String inStream2String(InputStream is, String charset)
			throws UnsupportedEncodingException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			byte[] buf = new byte[1024];
			int len = -1;
			while ((len = is.read(buf)) != -1) {
				baos.write(buf, 0, len);
			}
		} catch (Exception e) {

		} finally {
			try {
				baos.close();
			} catch (IOException e) {
			}
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
			}
		}
		return new String(baos.toByteArray(), charset);
	}

	public static void appendToFile(String dir, String fileName,
			CharSequence content) {
		try {
			File dirFile = new File(dir);
			if (!dirFile.exists()) {
				dirFile.mkdirs();
			}
			FileWriter writer = new FileWriter(dir + fileName, true);
			writer.write(content.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void appendLineToFile(String dir, String fileName,
			CharSequence content) {
		appendToFile(dir, fileName, content + "\n");
	}

	private static void close(FileOutputStream fos, FileInputStream fis) {
		if (fis != null) {
			try {
				fis.close();
				fis = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (fos != null) {
			try {
				fos.close();
				fis = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static byte[] gzip(byte[] data) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			GZIPOutputStream gzos = new GZIPOutputStream(baos);
			gzos.write(data);
			gzos.finish();
			gzos.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return baos.toByteArray();
	}

	public static byte[] ungzip(byte[] data) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ByteArrayInputStream bois = new ByteArrayInputStream(data);
			GZIPInputStream gzis = new GZIPInputStream(bois);
			byte[] buf = new byte[1024];
			int len;
			while ((len = gzis.read(buf)) > 0) {
				baos.write(buf, 0, len);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return baos.toByteArray();
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		String str = "usernameadasloupaiyunngitudechuangjikeonlinePricofflicountryNumbernePricee";
		System.out.println(str.getBytes().length);
		byte[] bytes = gzip(str.getBytes("utf-8"));
		System.out.println(bytes.length);
	}
}
