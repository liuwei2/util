package org.wei.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.wei.utils.exception.WrongDateFormatException;

public class DateUtil {
	private static String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static String nowDate2String() {
		return date2String(new Date());
	}

	public static String nowDate2String(String format) {
		DateFormat dft = new SimpleDateFormat(format);
		return dft.format(new Date());
	}

	public static String long2String(long l) {
		return long2String(l, DEFAULT_FORMAT);
	}

	public static String long2String(long l, String format) {
		DateFormat dft = new SimpleDateFormat(format);
		return dft.format(l);
	}

	public static String date2String(Date date) {
		DateFormat dft = new SimpleDateFormat(DEFAULT_FORMAT);
		return dft.format(date);
	}

	public static String date2String(Date date, String format) {
		DateFormat dft = new SimpleDateFormat(format);
		return dft.format(date);
	}

	public static String humanReadableString(Date date) {
		Date now = new Date();
		String nowStr = date2String(now);
		String[] nowDateAndTime = nowStr.split(" ");
		String nowDates = nowDateAndTime[0];
		int nowYear = Integer.parseInt(nowDates.split("-")[0]);
		int nowMonth = Integer.parseInt(nowDates.split("-")[1]);
		int nowDay = Integer.parseInt(nowDates.split("-")[2]);
		long interval = now.getTime() - date.getTime();
		int oneMinute = 60 * 1000;
		int oneHour = 60 * oneMinute;
		String dateStr = date2String(date);
		String[] dateAndTime = dateStr.split(" ");
		String dates = dateAndTime[0];
		int year = Integer.parseInt(dates.split("-")[0]);
		int month = Integer.parseInt(dates.split("-")[1]);
		int day = Integer.parseInt(dates.split("-")[2]);
		String times = dateAndTime[1];
		String hour = times.split(":")[0];
		String minite = times.split(":")[1];
		if (interval < oneMinute) {
			return "刚刚";
		} else if (interval < oneHour) {
			return interval / oneMinute + "分钟前";
		} else if (nowYear == year && nowMonth == month) {
			String result = hour + ":" + minite;
			if (nowDay == day) {
				return result;
			} else if (nowDay == day + 1) {
				return "昨天" + result;
			} else if (nowDay == day + 2) {
				return "前天" + result;
			}
		}
		return dateStr;
	}

	public static String humanReadableString(String date)
			throws WrongDateFormatException, ParseException {
		if (date == null) {
			return "";
		}
		if (!date.matches("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$")) {
			throw new WrongDateFormatException();
		}
		return humanReadableString(string2Date(date));
	}

	public static String humanReadableString2(Date date)
			throws WrongDateFormatException, ParseException {
		long interval = new Date().getTime() - date.getTime();
		long days = interval / (3600 * 24 * 1000);
		if (days > 1) {
			return date2String(date);
		}
		interval -= days * 3600 * 24 * 1000;
		long hours = interval / (3600 * 1000);
		interval -= hours * (3600 * 1000);
		long minutes = interval / (60 * 1000);
		interval -= minutes * (60 * 1000);
		long seconds = interval / 1000;
		StringBuilder sb = new StringBuilder();
		if (hours != 0) {
			sb.append(hours + "小时");
		}
		if (minutes != 0) {
			sb.append(minutes + "分钟");
		}
		sb.append(seconds + "秒钟前");
		return sb.toString();
	}

	public static String humanReadableString2(String date)
			throws WrongDateFormatException, ParseException {
		return humanReadableString2(string2Date(date));
	}

	public static Date string2Date(String date) throws ParseException {
		return string2Date(date, DEFAULT_FORMAT);
	}

	public static Date string2Date(String date, String format)
			throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.parse(date);
	}

	public static String now2HumanReadableString()
			throws WrongDateFormatException, ParseException {
		return humanReadableString(nowDate2String());
	}

	public static String changeOtherFormatString(String dateStr, String format)
			throws ParseException {
		Date date2 = string2Date(dateStr);
		return date2String(date2, format);
	}

	public static long getCurrentDaytime() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}

	public static long getCurrentMonthFirstDaytime() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}

	public static long getFirstDaytimeBy(int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}

	public static long getLastTimeBy(int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 0);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTimeInMillis();
	}

	public static int getOld(int bornYear, int bornMonth, int bornDay,
			int currentYear, int currentMonth, int currentDay) {
		boolean isFull = (currentMonth * 100 + currentDay)
				- (bornMonth * 100 + bornDay) >= 0;
		int old = currentYear - bornYear;
		if (!isFull) {
			old--;
		}
		return old;
	}

	public static void main(String[] args) throws WrongDateFormatException,
			ParseException {
		int old = getOld(1990, 6, 18, 2015, 6, 17);
		System.out.println(old);
	}
}
