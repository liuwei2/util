package org.wei.utils.validate;

public class RangeRule extends ARule {
	private int min;
	private int max;

	public RangeRule(int min, int max, String info) {
		super(info);
		this.min = min;
		this.max = max;
	}


	@Override
	public ValiResult check(String beCheckedStr) {
		int length = beCheckedStr.length();
		boolean isPass=length >= min && length <= max;
		ValiResult result=new ValiResult();
		result.setInfo(info);
		result.setPass(isPass);
		return result;
	}

}