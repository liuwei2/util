package org.wei.utils.validate;

public class EqualsRule extends ARule {
	private String srcStr;

	public EqualsRule(String srcStr, String info) {
		super(info);
		this.srcStr = srcStr;
	}

	@Override
	public ValiResult check(String beCheckedStr) {
		ValiResult valiResult = new ValiResult();
		valiResult.setInfo(info);
		if (srcStr == null) {
			boolean isPass = (beCheckedStr == null);
			valiResult.setPass(isPass);
			return valiResult;
		}
		boolean isPass = (srcStr.equals(beCheckedStr));
		valiResult.setPass(isPass);
		return valiResult;
	}

}
