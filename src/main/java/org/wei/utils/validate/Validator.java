package org.wei.utils.validate;

public class Validator {
	private ICheckble checkble;
	private String beCheckedStr;

	public static Validator create(String beCheckedStr, ICheckble checkble) {
		return new Validator(beCheckedStr, checkble);
	}

	private Validator(String beCheckedStr, ICheckble checkble) {
		this.beCheckedStr = beCheckedStr;
		this.checkble = checkble;
	}

	public ValiResult validate() {
		return checkble.check(beCheckedStr);
	}

	public Validator setCheckble(ICheckble checkble) {
		this.checkble = checkble;
		return this;
	}
}
