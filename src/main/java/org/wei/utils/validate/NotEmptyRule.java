package org.wei.utils.validate;

import org.wei.utils.StringUtil;

public class NotEmptyRule extends ARule {

	public NotEmptyRule(String info) {
		super(info);
	}

	@Override
	public ValiResult check(String beCheckedStr) {
		boolean isPass = !StringUtil.isEmptyString(beCheckedStr);
		ValiResult result = new ValiResult();
		result.setPass(isPass);
		result.setInfo(info);
		return result;
	}

}
