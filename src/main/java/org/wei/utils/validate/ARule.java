package org.wei.utils.validate;

public abstract class ARule implements ICheckble {
	protected String info;

	public ARule(String info) {
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

}
