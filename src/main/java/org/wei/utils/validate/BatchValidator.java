package org.wei.utils.validate;

import java.util.ArrayList;
import java.util.List;

public class BatchValidator {
	private List<Validator> validators = new ArrayList<Validator>();

	public static BatchValidator create(Validator validator) {
		return new BatchValidator(validator);
	}

	private BatchValidator(Validator validator) {
		addValidator(validator);
	}

	private BatchValidator() {

	}

	public BatchValidator addValidator(Validator validator) {
		validators.add(validator);
		return this;
	}

	public ValiResult validate() {
		for (Validator validator : validators) {
			ValiResult result = validator.validate();
			if (!result.isPass()) {
				return result;
			}
		}
		ValiResult result = new ValiResult();
		result.setPass(true);
		return result;
	}
}
