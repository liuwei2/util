package org.wei.utils.validate;

public class CountRule extends ARule {

	private int count;

	public CountRule(int count, String info) {
		super(info);
		this.count = count;
	}

	@Override
	public ValiResult check(String beCheckedStr) {
		ValiResult result = new ValiResult();
		boolean isPass = beCheckedStr.length() == count;
		result.setPass(isPass);
		result.setInfo(info);
		return result;
	}

}