package org.wei.utils.validate;

import java.util.ArrayList;
import java.util.List;

public class BatchRule implements ICheckble {
	private List<ARule> andRules = new ArrayList<ARule>();
	private List<ARule> orRules = new ArrayList<ARule>();

	public BatchRule (ARule rule){
		and(rule);
	}
	
	@Override
	public ValiResult check(String beCheckedStr) {
		// 检测所有or条件的规则,一条真就返回真
		for (ARule rule : orRules) {
			ValiResult result = rule.check(beCheckedStr);
			if (result.isPass()) {
				return result;
			}
		}
		// 检测所有and条件的规则，一条假就返回假
		for (ARule rule : andRules) {
			ValiResult result = rule.check(beCheckedStr);
			if (!result.isPass()) {
				return result;
			}
		}
		ValiResult result = new ValiResult();
		result.setPass(true);
		return result;
	}

	public BatchRule and(ARule rule) {
		andRules.add(rule);
		return this;
	}

	public BatchRule or(ARule rule) {
		orRules.add(rule);
		return this;
	}

}
