package org.wei.utils.validate;

import org.wei.utils.StringUtil;

public class IsNumberRule extends ARule {

	public IsNumberRule(String info) {
		super(info);
	}

	@Override
	public ValiResult check(String beCheckedStr) {
		ValiResult result = new ValiResult();
		result.setPass(StringUtil.isNumbers(beCheckedStr));
		result.setInfo(info);
		return result;
	}
}
