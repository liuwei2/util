package org.wei.utils.exception;

public class WrongDateFormatException extends Exception {

	private static final long serialVersionUID = 3996137471074955576L;
	
	public WrongDateFormatException(){
		super("错误的时间格式");
	}

}
