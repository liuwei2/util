package org.wei.utils;

public class MathUtil {
	public static double floor(double number, int digit) {
		int n = (int) Math.pow(10, digit);
		number = (int) (number * n);
		return number / n;
	}

	public static void main(String[] args) {
		System.out.println(floor(3, 8));
	}
}
