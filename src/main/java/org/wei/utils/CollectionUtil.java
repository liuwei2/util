package org.wei.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CollectionUtil<T> {
	public List<T> addToFixSize(List<T> oriList, List<T> needToAddList,
			int size, Comparator<T> comparator) {
		if (oriList == null) {
			oriList = new ArrayList<T>();
		}
		oriList.addAll(needToAddList);
		Collections.sort(oriList, comparator);
		if (oriList.size() < size) {
			size = oriList.size();
		}
		return oriList.subList(0, size);
	}

	public List<T> addToFixSize(List<T> oriList, List<T> needToAddList, int size) {
		if (oriList == null) {
			oriList = new ArrayList<T>();
		}
		oriList.addAll(needToAddList);
		if (oriList.size() < size) {
			size = oriList.size();
		}
		return oriList.subList(0, size);
	}
}
